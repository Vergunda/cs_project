'use strict';
import React from 'react';
import { render } from 'react-dom';
import configCore from '../config/config';
import RootModule from '../modules/rootModule/RootModule.jsx';

document.addEventListener('DOMContentLoaded', () => {
    init();
});

function init() {
    document.title = configCore.title;
    setFavIcon(configCore.favIcon);
    render(<RootModule/>, document.getElementById('root'));
}

export function setFavIcon(src) {
    if (!src) {
        return false;
    }

    const favicon = document.createElement('link');

    favicon.rel = 'icon';
    favicon.href = src;
    document.head.appendChild(favicon);
}
