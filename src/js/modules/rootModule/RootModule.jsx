import React from 'react';
import PropTypes from 'prop-types';
import PureComponent from '../../base/PureComponent.jsx';
import ErrorBoundary from '../../decorators/ErrorBoundary.jsx';

@ErrorBoundary
export default class RootModule extends PureComponent {
    render() {
        return (
            <React.Fragment>
                <div children={'TEST NODE'}/>
            </React.Fragment>
        );
    }
}

RootModule.propTypes = {
    propAny: PropTypes.any,
    propBool: PropTypes.bool,
    propFunc: PropTypes.func,
    propArray: PropTypes.array,
    propNumber: PropTypes.number,
    propString: PropTypes.string,
    propObject: PropTypes.object,
    propRequired: PropTypes.object.isRequired,
};
