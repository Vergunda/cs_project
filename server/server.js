import http from 'http';
import url from 'url';
import qs from 'querystring';
import mysql from 'mysql2/promise';

export default class Server {
    httpServer = null;
    errorCodes = {
        sqlError: 'SQL_ERROR',
        noSuchCommand: 'NO_SUCH_COMMAND',
        noSuchServerCommand: 'NO_SUCH_SERVER_COMMAND',
    };
    dbConnection = null;
    contentTypes = {
        json: { 'Content-Type': 'application/json' },
    };
    httpServerPort = 1010;
    postProcessors = {};
    getProcessors = {};

    initialize = async () => {
        await this.connectToDb();
        this.runHttpServer();
        this.initRequestProcessors();
    };

    connectToDb = async () => {
        try {
            this.dbConnection = await mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: '',
                database: 'dbName',
            });
            this.setConnectionErrorHandler(this.dbConnection);
        } catch (err) {
            console.error(`DB connection failed: ${err.code}`);
            this.dbConnection = null;
            return false;
        }
    };

    setConnectionErrorHandler = dbConnection => {
        dbConnection.on('error', async err => await this.dbErrorHandler(err));
    };

    dbErrorHandler = async error => {
        if (error.code === 'PROTOCOL_CONNECTION_LOST') {
            this.dbConnection.destroy();
            this.dbConnection = null;
            await this.connectToDb();
        }
    };

    runHttpServer = () => {
        if (!this.dbConnection) {
            console.error(new Error(`Failed to create server: DB connection is not created.`));
            return false;
        }

        this.httpServer = http.createServer((req, res) => this.processRequest(req, res));
        this.httpServer.listen(this.httpServerPort, this.httpServerListenLog);
    };

    initRequestProcessors = () => {
        if (!this.dbConnection
            || !this.httpServer) {
            console.error(`Failed to init processors: DB connection or HTTP server is not created.`);

            return false;
        }

        this.postProcessors = {
            test: this.testProcessor,
        };
        this.getProcessors = {
            test: this.testProcessor,
        };
    };

    httpServerListenLog = () => {
        console.log(new Date() + '\nServer is listening on port ' + this.httpServerPort);
    };

    processRequest = async (req, res) => {
        const requestQuery = url.parse(req.url, true);
        const requestTarget = requestQuery.pathname.substring(1);
        const requestParams = requestQuery.query;

        switch (req.method) {
            case 'POST':
                let body = '';

                req.on('data', data => {
                    body += data;

                    if (body.length > 1e6) {
                        req.connection.destroy();
                    }
                });

                req.on('end', async () => {
                    const postArgs = qs.parse(body);

                    if (this.postProcessors[requestTarget]) {
                        const responseContentPost = await this.postProcessors[requestTarget](postArgs);
                        this.sendResponse(res, this.contentTypes.json, JSON.stringify(responseContentPost));
                    } else {
                        this.sendResponse(res, this.contentTypes.json, JSON.stringify(this.getErrorResponseObject('noSuchCommand', null)));
                    }
                });
                break;

            case 'GET':
                if (this.getProcessors[requestTarget]) {
                    const responseContentGet = await this.getProcessors[requestTarget](requestParams);
                    this.sendResponse(res, this.contentTypes.json, JSON.stringify(responseContentGet));
                } else {
                    this.sendResponse(res, this.contentTypes.json, JSON.stringify(this.getErrorResponseObject('noSuchCommand', null)));
                }
                break;

            default:
                this.sendResponse(res, this.contentTypes.json, JSON.stringify(this.getErrorResponseObject('noSuchServerCommand', null)));
        }
    };

    getErrorResponseObject = (command, errorData) => {
        return {
            isError: true,
            code: this.errorCodes[command],
            data: errorData || null,
        };
    };

    sendResponse = (response, header, data) => {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Request-Method', '*');
        response.setHeader('Access-Control-Allow-Methods', '*');
        response.setHeader('Access-Control-Allow-Headers', '*');
        response.writeHead(200, header);
        response.write(data);
        response.end();
    };

    testProcessor = async queryArgs => {
        console.log('queryArgs:', queryArgs);
        let testQueryResult = null;
        try {
            const [rows] = await this.dbConnection.execute(`SQL QUERY`);
            testQueryResult = rows;
        } catch (err) {
            return this.getErrorResponseObject('sqlError', `${err.code}: ${err.sqlMessage}`);
        }

        return {
            isError: false,
            data: [{ ...testQueryResult[0] }],
        };
    };
}

const isProduction = process.env.NODE_ENV === 'production';
console.log(`Production status: ${isProduction}.`);

if (isProduction) {
    (async () => {
        const instance = new Server();
        await instance.initialize();
    })();
}
